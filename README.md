

# LIS4381 Mobile Web Application Development

## Marquez Mobley

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Android app
    - Provide screenshots of completed app

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create ERD based upon business rules
    - Provide screenshots of completed ERD
    - Provide DB resource links

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create online portfolio
    - Create Basic client-side validation
    - Answer PHP/MySQL Questions
    - Complete skill sets

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Clone A4 Files into A5 directory
    - Turn off client-side validation and add server-side validation
    - Develop user interface to view, create, edit and delete records from database
    - Complete skill sets

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create personal business card via mobile app development
    - Complete skill sets
    - Answer PHP/MySQL questions

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Clone A5 Files into P2 directory
    - Add server-side validation
    - Add edit/delete functionality
    - Create RSS Feed link

