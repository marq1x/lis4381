

# LIS4381 Mobile Web Application Development

## Marquez Mobley

### Assignment 1 # Requirements:

*Three Parts*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation My PHP Installation
* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* git commands w/ short descriptions
* Bitbucket repo links a)this assignment and b)the completed tutorials above (bitbucketstationlocations and my teamquotes)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:


1. git init - Creates a new or convert a folder into a repository
2. git status - Displays the status of a working directory and wheter it is up to date
3. git add - let Git know what file changes you want saved in the next commit
4. git commit - used after "git add ." to save file changes to your local repository 
5. git push - used to upload your local repository to your remote repository
6. git pull - used to download content from a remote repository to a local repository
7. git remote -v - shows remote repos you are connected to


#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/Screenshot_ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/Screenshot_JavaHello.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/Screenshot_AndroidStudio.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/marq1x/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
