<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Marquez Mobley.">
    <link rel="icon" href="favicon.ico">

		<title>4381 - Assignment 1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Working in the confines of a version control system to store and maintain files; 
					I learned how to use the Git distributed version control system and the Bitbucket service for managing repositories as well as the git command-line tool.
				</p>

				<h4>Screenshot of running java Hello</h4>
				<img src="img/Screenshot_JavaHello.png" class="img-responsive center-block" alt="Screenshot of running java Hello">

				<h4>Screenshot of Android Studio - My First App</h4>
				<img src="img/Screenshot_AndroidStudio.png" class="img-responsive center-block" alt="Screenshot of Android Studio - My First App">

				<h4>Screenshot of AMPPS running</h4>
				<img src="img/Screenshot_ampps.png" class="img-responsive center-block" alt="Screenshot of AMPPS running">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
