    

# Course LIS4381

## Marquez Mobley

### Assignment 2 # Requirements:

*Three Parts:*

1. Create a mobile recipe app using Android Studio
2. Change the background and text colors in both activities, or use background image
3. Complete Skillsets 1-3

#### README.md file should include the following items:

* Screenshots of User Interface
* Screenshots of Skillsets 1-3

#### Assignment Screenshots:

*Screenshot of User Interface 1*:

![User Interface 1 Screenshot](img/Recipe1.png)

*Screenshot of User Interface 2*:

![User Interface 2 Screenshot](img/Recipe2.png)

*Screenshot of Skill Set 1*:

![Skill Set 1, Even or Odd Screenshot](img/Q1_EvenOrOdd.png)

*Screenshot of Skill Set 2*:

![Skill Set 2, Largest of Two Integers Screenshot](img/Q2_Largest_of_Two_Integers.png)

*Screenshot of Skill Set 3*:

![Skill Set 3, Q3_Arrays_And_Loops Screenshot](img/Q3_Arrays_And_Loops.png)
