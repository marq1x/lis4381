<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Marquez Mobley.">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> I created a mobile application to provide a recipe and I also completed skill sets to show off my java coding skills
				</p>

				<h4>Screenshot of User Interface 1</h4>
				<img src="img/Recipe1.png" class="img-responsive center-block" alt="Screenshot of User Interface 1">

				<h4>Screenshot of User Interface 2</h4>
				<img src="img/Recipe2.png" class="img-responsive center-block" alt="Screenshot of User Interface 2">

				<h4>Screenshot of Skill Set 1</h4>
				<img src="img/Q1_EvenOrOdd.png" class="img-responsive center-block" alt="Screenshot of Skill Set 1">
				
				<h4>Screenshot of Skill Set 2</h4>
				<img src="img/Q2_Largest_of_Two_Integers.png" class="img-responsive center-block" alt="Screenshot of Skill Set 2">

				<h4>Screenshot of Skill Set 3</h4>
				<img src="img/Q3_Arrays_And_Loops.png" class="img-responsive center-block" alt="Screenshot of Skill Set 3">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
