

# Course LIS4381

## Marquez Mobley

### Assignment 3 # Requirements:

*Three Parts:*

1. Create a mobile web application for a pet store
2. Create and connect database for mobile web application
3. Answer questions from PHP/MySQL books, Chapter 5 and 6

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running applications opening user interface
* Screenshot of running applications processing user input
* Screenshots of 10 records for each table
* Screenshots of skill sets 4-6
* Links to the following files: a3.mwb and a3.sql

#### Assignment Screenshots:

*Screenshot of ERD*:

![Screenshot of ERD](img/a3_erd.png)

*Screenshot of running applications opening user interface*:

![Screenshot of running applications opening user interface](img/on_startup.png)

*Screenshot of running applications processing user input*:

![Screenshot of running applications processing user input](img/processed.png)

*Screenshots of 10 records for each table*:

![Screenshots of 10 records for each table](img/a3_table1.png)
![Screenshots of 10 records for each table](img/a3_table2.png)
![Screenshots of 10 records for each table](img/a3_table3.png)

*Screenshots of skill set 4*:

![Screenshots of skill set 4](img/Q4_Decision_Structures.png)

*Screenshots of skill set 5*:

![Screenshots of skill set 5](img/Q5_Random_Number_Generator.png)

*Screenshots of skill set 6*:

![Screenshots of skill set 6](img/Q6_Methods.png)


#### File Links:

*sql file:*
[sql file](docs/a3.sql "sql file")

*mwb file:*
[mwb file](docs/a3.mwb "mwb file")
