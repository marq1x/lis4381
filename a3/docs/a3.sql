-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mm20ov
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mm20ov` ;

-- -----------------------------------------------------
-- Schema mm20ov
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mm20ov` DEFAULT CHARACTER SET utf8mb4 ;
SHOW WARNINGS;
USE `mm20ov` ;

-- -----------------------------------------------------
-- Table `mm20ov`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mm20ov`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mm20ov`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT ZEROFILL NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) UNSIGNED NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mm20ov`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mm20ov`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mm20ov`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT ZEROFILL NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(8,2) NOT NULL,
  `cus_total_sales` DECIMAL(8,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mm20ov`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mm20ov`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mm20ov`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC) VISIBLE,
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC) VISIBLE,
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `mm20ov`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `mm20ov`.`customer` (`cus_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `mm20ov`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `mm20ov`;
INSERT INTO `mm20ov`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'ALEXPETS', '122 Main st', 'NY', 'NY', 12567, 8181111111, 'alexpets@gmail.com', 'www.ALEXPETS.com', 1000, '1');
INSERT INTO `mm20ov`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'BARKPETS', '222 Main st', 'NY', 'NY', 12567, 8182222222, 'barkpets@gmail.com', 'www.BARKPETS.com', 1000, '2');
INSERT INTO `mm20ov`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'CAMPETS', '322 Main st', 'NY', 'NY', 12567, 8183333333, 'campets@gmail.com', 'www.CAMPETS.com', 1000, '3');
INSERT INTO `mm20ov`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'DILLPETS', '422 Main st', 'NY', 'NY', 12567, 8184444444, 'dillpets@gmail.com', 'www.DILLPETS.com', 1000, '4');
INSERT INTO `mm20ov`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'ERICPETS', '522 Main st', 'NY', 'NY', 12567, 8185555555, 'ericpets@gmail.com', 'www.ERICPETS.com', 1000, '5');
INSERT INTO `mm20ov`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'FINPETS', '622 Main st', 'NY', 'NY', 12567, 8186666666, 'finpets@gmail.com', 'www.FINPETS.com', 1000, '6');
INSERT INTO `mm20ov`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'GALPETS', '722 Main st', 'NY', 'NY', 12567, 8187777777, 'galpets@gmail.com', 'www.GALPETS.com', 1000, '7');
INSERT INTO `mm20ov`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'HAMPETS', '822 Main st', 'NY', 'NY', 12567, 8188888888, 'hampets@gmail.com', 'www.HAMPETS.com', 1000, '8');
INSERT INTO `mm20ov`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'IKEPETS', '922 Main st', 'NY', 'NY', 12567, 8189999999, 'ikepets@gmail.com', 'www.IKEPETS.com', 1000, '9');
INSERT INTO `mm20ov`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'JIMPETS', '1022 Main st', 'NY', 'NY', 12567, 8181010101, 'jimpets@gmail.com', 'www.JIMPETS.com', 1000, '10');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mm20ov`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `mm20ov`;
INSERT INTO `mm20ov`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Alex', 'Apples', '122 Highway', 'Jacksonville', 'FL', 32218, 9041111111, 'Alex@gmail.com', 10, 20, '1');
INSERT INTO `mm20ov`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Bark', 'Babies', '222 Highway', 'Jacksonville', 'FL', 32218, 9041111112, 'Bark@gmail.com', 20, 30, '2');
INSERT INTO `mm20ov`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Cam', 'Cats', '322 Highway', 'Jacksonville', 'FL', 32218, 9041111113, 'Cam@gmail.com', 30, 40, '3');
INSERT INTO `mm20ov`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Tyler', 'Tiger', '422 Highway', 'Jacksonville', 'FL', 32218, 9041111114, 'Tyler@gmail.com', 40, 50, '4');
INSERT INTO `mm20ov`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Jason', 'Johnson', '522 Highway', 'Jacksonville', 'FL', 32218, 9041111115, 'Jason@gmail.com', 50, 60, '5');
INSERT INTO `mm20ov`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Sam', 'Sake', '622 Highway', 'Jacksonville', 'FL', 32218, 9041111116, 'Sam@gmail.com', 60, 70, '6');
INSERT INTO `mm20ov`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Bill', 'Battles', '722 Highway', 'Jacksonville', 'FL', 32218, 9041111117, 'Bill@gmail.com', 70, 80, '7');
INSERT INTO `mm20ov`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Mark', 'Mason', '822 Highway', 'Jacksonville', 'FL', 32218, 9041111118, 'Mark@gmail.com', 80, 90, '8');
INSERT INTO `mm20ov`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Tyrone', 'Target', '922 Highway', 'Jacksonville', 'FL', 32218, 9041111119, 'Tyrone@gmail.com', 90, 100, '9');
INSERT INTO `mm20ov`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'James', 'Jackson', '1022 Highway', 'Jacksonville', 'FL', 32218, 9041111110, 'James@gmail.com', 103.45, 103.45, '10');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mm20ov`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `mm20ov`;
INSERT INTO `mm20ov`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 2, 'Monkey', 'm', 100.00, 300, 1, 'black', '2022-02-01', 'y', 'n', '1');
INSERT INTO `mm20ov`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, 3, 'Dog', 'f', 202.12, 300, 1, 'black', '2022-02-02', 'y', 'y', '2');
INSERT INTO `mm20ov`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 4, 'Cat', 'm', 121.38, 300, 1, 'black', '2022-02-03', 'y', 'n', '3');
INSERT INTO `mm20ov`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 4, 'Fish', 'f', 103.12, 300, 1, 'black', '2022-02-04', 'y', 'y', '4');
INSERT INTO `mm20ov`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 6, 'Snake', 'm', 122.34, 300, 1, 'black', '2022-02-05', 'y', 'n', '5');
INSERT INTO `mm20ov`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 6, 7, 'Cow', 'f', 192.45, 300, 1, 'brown', '2022-02-06', 'y', 'y', '6');
INSERT INTO `mm20ov`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 7, 8, 'Bird', 'm', 132.21, 300, 1, 'brown', '2022-02-07', 'y', 'n', '7');
INSERT INTO `mm20ov`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 8, 9, 'Lizard', 'f', 143.23, 300, 1, 'brown', '2022-02-08', 'y', 'y', '8');
INSERT INTO `mm20ov`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 9, 10, 'Chicken', 'm', 111.27, 300, 1, 'brown', '2022-02-09', 'y', 'y', '9');
INSERT INTO `mm20ov`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 10, 10, 'Pig', 'f', 129.93, 300, 1, 'brown', '2022-02-10', 'y', 'n', '10');

COMMIT;

