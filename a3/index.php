<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Marquez Mobley.">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Created a mobile web application for a pet store. Created and connected a database for the mobile web application.
				</p>

				<h4>Screenshot of ERD</h4>
				<img src="img/a3_erd.png" class="img-responsive center-block" alt="Screenshot of ERD">

				<h4>Screenshot of running applications opening user interface</h4>
				<img src="img/on_startup.png" class="img-responsive center-block" alt="Screenshot of running applications opening user interface">

				<h4>Screenshot of running applications processing user input</h4>
				<img src="img/processed.png" class="img-responsive center-block" alt="Screenshot of running applications processing user input">
				
				<h4>Screenshots of 10 records for each table</h4>
				<img src="img/a3_table1.png" class="img-responsive center-block" alt="Screenshots of 10 records for each table">
				<img src="img/a3_table2.png" class="img-responsive center-block" alt="Screenshots of 10 records for each table">
				<img src="img/a3_table3.png" class="img-responsive center-block" alt="Screenshots of 10 records for each table">

				<h4>Screenshots of skill set 4-6</h4>
				<img src="img/Q4_Decision_Structures.png" class="img-responsive center-block" alt="Screenshots of skill set 4-6">
				<img src="img/Q5_Random_Number_Generator.png" class="img-responsive center-block" alt="Screenshots of skill set 4-6">
				<img src="img/Q6_Methods.png" class="img-responsive center-block" alt="Screenshots of skill set 4-6">

				<h4>File Links</h4>
				<h4><a href="docs/a3.sql">Sql file</a></h4>
				<h4><a href="docs/a3.mwb">Mwb file</a></h4>

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
