

# Course LIS4381

## Marquez Mobley

### Assignment 5 # Requirements:

1. Clone A4 Files into A5 directory
2. Turn off client-side validation and add server-side validation
3. Develop user interface to view, create, edit and delete records from database
4. Complete skill sets

#### README.md file should include the following items:

* Screenshots of user interface
* Screenshots of skill sets

#### Assignment Screenshots:

*Screenshot of user interface; entering invalid info*:

![entering invalid info](img/Pre_error.png)

*Screenshot of user interface; result of invalid info*:

![result of invalid info](img/Error.png)

*Screenshot of user interface; entering valid info*:

![entering valid info](img/Pre_add.png)

*Screenshot of user interface; result valid info*:

![result of valid info](img/Add.png)

*Screenshot of skill set 13*:

![Skill set 13](img/Q13.png)

*Screenshot of skill set 14*:

![Skill set 14](img/Q141.png)
![Skill set 14](img/Q142.png)
![Skill set 14](img/Q143.png)
![Skill set 14](img/Q144.png)

*Screenshot of skill set 15*:

![Skill set 15](img/Q151.png)
![Skill set 15](img/Q152.png)


