

# Course LIS4381

## Marquez Mobley

### Project 1 # Requirements:

*Three tasks:*

1. Create personal business card via mobile app development
2. Complete skill sets
3. Answer PHP/MySQL questions

#### README.md file should include the following items:

* Screenshot of running applications first user interface
* Screenshot of running applications second user interface
* Screenshot of skill sets



#### Assignment Screenshots:

*Screenshot of Android Studio - Business Card*:

![Business Card](img/card1.png)
![Business Card](img/card2.png)

*Screenshot of skill sets*:

![skill sets](img/Q8_LargetThreeNumbers.png)
![skill sets](img/Q7_Random_Number_Generator_Data_Validation.png)
![skill sets](img/Q9_Array_Runtime_Data_Validation.png)


