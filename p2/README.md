

# Course LIS4381

## Marquez Mobley

### Project 2 # Requirements:

1. Clone A5 Files into P2 directory
2. Add server-side validation
3. Add edit/delete functionality
4. Create RSS Feed link

#### README.md file should include the following items:

* Screenshots of user interface

#### Assignment Screenshots:

*Screenshot of Home Page*:

![entering invalid info](img/welcome.png)

*Screenshot of user interface*:

![entering invalid info](img/edit1.png)

*Screenshot of user interface; Editing Petstore*:

![result of invalid info](img/edit2.png)

*Screenshot of user interface; Failed Entry*:

![entering valid info](img/Failed.png)

*Screenshot of user interface; Valid Entry*:

![result of valid info](img/Passed.png)

*Screenshot of user interface; Deletion Prompt*:

![entering valid info](img/delete1.png)

*Screenshot of user interface; Record Deleted*:

![result of valid info](img/delete2.png)

*Screenshot of user interface; RSS Feed*:

![result of valid info](img/rss.png)


