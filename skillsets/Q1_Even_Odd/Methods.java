import java.util.Scanner;

public class Methods
{
    //Introduction to assignment
    public static void getRequirements()
    {
        // Introduction
        System.out.println("Developer: Marquez Mobley");
        System.out.println("Program that evaluates integers as even or odd.");
        System.out.println(""); //Blank line
    }
    //The Program doing the work
    public static void evaluateNumber()
    {
        //initialize variables, create Scanner object, ask for user input
        int userinput = 0;
        System.out.print("Enter integer: ");
        Scanner sc = new Scanner(System.in);
        userinput = sc.nextInt();

        //Determine if the interger is even or odd
        if (userinput % 2 == 0)
        {
            System.out.println(userinput + " is an even number.");
        }
        else
        {
            System.out.println(userinput + " is an odd number.");
        }
    }
}