class Main 
{
    public static void main(String args[]) 
    {
      //code to be executed
      Methods.getRequirements();

      System.out.println("***Call static (no object) void (non-value returning) method:*** ");
      Methods.largestNumber();

      //Declare variables and create Scanner object
      int myNum1=0, myNum2=0;

      //calling method from method file
      System.out.print("Enter first integer: ");
      myNum1 = Methods.getNum();

      System.out.print("Enter second integer: ");
      myNum2 = Methods.getNum();

      //call void method passing user input
      Methods.evaluateNumber(myNum1, myNum2);
    }
}