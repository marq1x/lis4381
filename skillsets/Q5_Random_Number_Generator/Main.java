import java.util.Arrays;

class Main 
{
  public static void main(String args[]) 
  {
    //code to be executed
    Methods.getRequirements();

    int[] userArray = Methods.createArray(); 
    
    Methods.generatePseudoRandomNumbers(userArray);
  }
}    