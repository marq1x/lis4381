

# Course LIS4381

## Marquez Mobley

### Assignment 4 # Requirements:

1. Create online portfolio
2. Create Basic client-side validation
3. Answer PHP/MySQL Questions
4. Complete skill sets

#### README.md file should include the following items:

* Screenshot of online portfolio's main page
* Screenshot of Failed validation
* Screenshot of passed validation
* Screenshots of skill sets 10-12
* Link to local lis4381 web app:

#### Assignment Screenshots:

*Screenshot of online portfolio's main page*

![Screenshot of online portfolio's main page](img/1.png)

*Screenshot of Failed validation*:

![Screenshot of Failed validation](img/2.png)

*Screenshot of passed validation*:

![Screenshot of passed validation](img/3.png)

*Screenshot of skill set 10*:
![Screenshot of skill sets](img/Q10_ArrayList.png)
*Screenshot of skill set 11*:
![Screenshot of skill sets](img/Q11_Alpha_Numeric_Special.png)
*Screenshot of skill set 12*:
![Screenshot of skill sets](img/Q12_Temperature_Conversion.png)


#### Link to Local Lis4381 Web App:

*Link is below:*
[Link to Local Lis4381 Web App](http://localhost:8080/repos/lis4381/index.php "Link to Local Lis4381 Web App")
